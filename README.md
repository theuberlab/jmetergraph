# jmetergraphs

Agregates a number of jmeter log files and produces graphs. Useful when automating jmeter tests across multiple hosts.

Forking some code I updated and had to find a home for several years ago.

I would like to make this a bit more comprehensive potentially by wrapping the graphs with click throughs, producing rrd files or the like.


Original script by Christoph Meissner (email unknown)

This script was fetched from a jmeter wiki page on log analysis which at one point resided here: 
https://wiki.apache.org/jmeter/LogAnalysis

Originally found http://wiki.apache.org/jakarta-jmeter-data/attachments/LogAnalysis/attachments/jmetergraph.pl

Fixes by George Barnett (george@alink.co.za)

Small patch (from mhardy@tkdevvm) 

html output added by Aaron Forster (jmeter@forstersfreehold.com)

